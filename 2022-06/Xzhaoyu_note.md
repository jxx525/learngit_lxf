**致谢**

感谢老师的 git 教程，并且给我们发起 Pull Request 的机会。下面是我看完老师的教程后，总结的指令速查笔记。 

**笔记**

初始化仓库 `git init`

添加文件到本地仓库 `git add<file>` `git commit -m <message>`

查看工作区当前状态 `git status`

对比文件的修改 `git diff`

查看历史提交记录 `git log`

版本之间穿梭 `git reset --hard commit_id`

`HEAD` 指向的是当前版本

已回滚到过去版本，如果想跳转到新版本，用 `git reflog` 查看命令历史

丢弃工作区的修改 `git checkout -- file`

丢弃暂存区的修改，先 `git reset HEAD <file>` ，再丢弃工作区修改

创建SSH Key `ssh-keygen -t rsa -C "youremail@example.com"` ，一路回车就行

所创建的公私钥默认在 `~/.ssh` 中，`id_rsa` 是私钥， `id_rsa.pub` 是公钥。

关联远程仓库 `git remote add origin XXXX` .其中 	origin 是远程仓库的别名，也可以换别的名字， XXXX 是仓库地址。

关联后，第一次用 `git push -u origin master` master分支推给远程仓库。

克隆远程仓库 `git clone XXXX`

查看分支 `git branch`

创建分支 `git branch name`

切换分支 `git checkout name` 或 `git switch name`

创建并切换分支 `git checkout -b name` 或 `git switch -c name`

把别的分支与当前所在分支合并 `git merge name`

删除分支 `git branch -d name`

当分支合并产生冲突时，通过 `git status` 查看冲突文件，具体有冲突的行用 `<<<<<<<`、`=======`、 `>>>>>>>` 标记出来了。手动消除冲突后，再 add、commit 就行了

查看分支合并图 `git log --graph`

fast forward 模式会优化分支合并，简单来说，就是直接把master 指向 HEAD/dev。这样以后看不出来曾经有分支合并的过程，如果想要保留这个分支的过程，用 `git merge --no-ff -m 'merge with no-ff' dev` 来合并。

假设当前手头的工作没做完，就要紧急去另一个分支干活，例如去修 bug 。做到一半的工作不能 commit，就用 `git stash` 存一下工作现场，等 bug 修完后，再 `git stash list` 查看存的工作现场，`git stash apply` 恢复， `git stash drop` 删除工作现场。也可以用 `git stash pop` 恢复并删除工作现场。

假如刚刚紧急给 master 分支修了个 bug ，dev 分支肯定也有这个 bug ，就用 `git cherry-pick <commit>` 把 bug 的修改给复制过来，不需要重复劳动。

如果某个分支删不掉（比如没 merge 就要删），就用 `git branch -D <name>` 来强制删除。

查看远程仓库信息 `git remote -v`

抓取远程提交 `git pull`

本地创建和远程分支对应的分支 `git checkout -b branch-name origin/banch-name` ，本地和远程分支名称最好一致。

建立本地和远程分支的关联 `git branch --set-upstream branch-name origin/branch-name`

将本地未 push 的分叉提交历史整理成直线 `git rebase`

在当前分支最新的 commit 上打标签 `git tag tag_name`

当前分支的某个 commit 上打标签 `git tag tag_name commit_id`

带文字说明的标签 `git tag -a tag_name -m 'information' commit_id`

查看某个标签 `git show tag_name`，查看所有标签 `git tag`

删除本地标签 `git tag -d tag_id`

删除远程标签 `git push origin :refs/tags/<tagname>`

推送一个本地标签 `git push origin <tagname>`

推送所有本地未推送过的标签 `git push origin --tags`

参与开源项目：先fork别人的仓库到自己的远程仓库，然后clone到自己本地（一定要从自己的远程仓库中clone，否则没有推送权限），自己本地修改完了以后，push到自己远程仓库，然后发起 pull 请求。

忽略特殊文件：在 git 工作根目录下，创建一个 `.gitignore` 文件，把要忽略的文件名填进去就可以了。比如要忽略后缀为 `.exe` 的文件，就在 `.gitignore` 中添加 `*.exe` 一行就行了。

如果我想忽略所有的 exe 文件，但是只有一个 `special.exe` 还是希望 git 能管理它的版本，那就在 `.gitignore` 中再添加一行 `!special.exe`。或者用 `git add -f special.exe` 强制添加到git。

`.gitignore` 要交给 git 去管理。

假设我在 `.gitignore` 写了很多行，现在我突然发现某个 `a.exe` 它居然不给我管理，用 `git check-ignore -v a.exe` 来定位究竟是 `.gitignore` 中哪一行的规则导致的这个问题。

`.gitignore` 的模板 [链接](https://github.com/github/gitignore)

在 `.git/config` (当前仓库)或 `~/.gitconfig` 中配置全局参数与快捷键。例如：  ~~用了都说好~~
```
[alias]
	lg = log --color --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit
```

git 图形化界面工具 [SourceTree](https://www.sourcetreeapp.com/)

[git教程](https://www.liaoxuefeng.com/wiki/896043488029600)
